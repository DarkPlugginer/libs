#include "Arduino.h"
#include "RotaryEncoder.h"

const int8_t KNOBDIR[] = {
    0, -1, 1, 0,
    1, 0, 0, -1,
    -1, 0, 0, 1,
    0, 1, -1, 0};

struct Encoder
{
  byte pin1;
  byte pin2;

  volatile int8_t _oldState;
  volatile long _position;
  volatile float _positionExt;
};

Encoder encoders[2];

RotaryEncoder::RotaryEncoder(byte pin1, byte pin2, byte pin3, byte pin4)
{
  encoders[0].pin1 = pin1;
  encoders[0].pin2 = pin2;

  encoders[1].pin1 = pin3;
  encoders[1].pin2 = pin4;

  for (byte i = 0; i < sizeof(encoders) / sizeof(encoders[0]); i++)
  {
    pinMode(encoders[i].pin1, LOW);
    digitalWrite(encoders[i].pin1, HIGH);

    pinMode(encoders[i].pin2, LOW);
    digitalWrite(encoders[i].pin2, HIGH);

    encoders[i]._oldState = 3;
    encoders[i]._position = 0;
    encoders[i]._positionExt = 0;
  }
}

float RotaryEncoder::getPosition(byte encoder_index)
{
  return encoders[encoder_index]._positionExt;
}

void RotaryEncoder::setPosition(byte encoder_index, long newPosition)
{
  encoders[encoder_index]._position = ((newPosition << 2) | (encoders[encoder_index]._position & 0x03L));
  encoders[encoder_index]._positionExt = newPosition;
}

void RotaryEncoder::tick(byte encoder_index)
{
  int sig1 = digitalRead(encoders[encoder_index].pin1);
  int sig2 = digitalRead(encoders[encoder_index].pin2);
  int8_t thisState = sig1 | (sig2 << 1);

  if (encoders[encoder_index]._oldState != thisState)
  {
    encoders[encoder_index]._position += KNOBDIR[thisState | (encoders[encoder_index]._oldState << 2)];

    if (thisState == 3)
      encoders[encoder_index]._positionExt = encoders[encoder_index]._position >> 2;

    encoders[encoder_index]._oldState = thisState;
  }
}

void RotaryEncoder::tick()
{
  tick(0);
  tick(1);
}