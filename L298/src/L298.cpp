/*
   Copyright (©)

   Projeto desenvolvido por Miguel Rodrigues
   Não remova este quote

   Modificado em: 13/10/18 17:39
*/

#include "L298.h"

/**
   Direções
   0 - frente
   1 - trás
   2 - esquerda
   3 - direita
*/

/**
   Encoders
   0 - esquerdo
   1 - direito
*/

/**
   Motores
   0 - esquerdo
   1 - direito
*/

/**
   Estrutura de dados dos motores
*/
struct Motor
{
  byte in1;
  byte in2;
};

/**
   Estrutura de dados do encoder
*/
struct EncoderData
{
  float pos;
  float npos;
  float voltas;
};

/**
   Declaração de arrays para armazedar dados dos motores e dos encoders
*/
Motor motors[2];
EncoderData datas[2];

/**
   Instancia da classe 'RotaryEncoder'
*/
RotaryEncoder encoder(2, 4, 8, 7);

/**
   Instancia da classe 'Ultrasonic'
*/
Ultrasonic sensor(A0, A1, A2, A3, A4, A5);

/**
   Construtor da classe 'L298'
   @param in1 - In1 da ponte h
   @param in2 - In2 da ponte h
   @param in3 - In3 da ponte h
   @param in4 - In4 da ponte h
*/
L298::L298(byte in1, byte in2, byte in3, byte in4)
{
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  motors[0].in1 = in1;
  motors[0].in2 = in2;

  motors[1].in1 = in3;
  motors[1].in2 = in4;
}

/**
   Método usado para andar para frente ou para trás, com ou sem uma distância determinada
   @param direction - direção a se locomover
   @param speed - velocidade a ser aplicada nos motores
   @param distance - distância a ser percorrida caso maior que 0
*/
void L298::andar(byte direction, byte speed, float distance, bool zero)
{
  switch (direction)
  {
  case 0:
    if (distance == 0)
    {
      while (dis_sensor(0) < 15)
      {
        setup_motors(35, 0, 0, 150);
      }

      if (dis_sensor(1) <= 3)
      {
        while (dis_sensor(1) < 6)
        {
          setup_motors(0, 130, 130, 0);
        }

        if (dis_sensor(0) > dis_sensor(2))
        {
          rodar(130, -30);
        }
        else if (dis_sensor(2) > dis_sensor(0))
        {
          rodar(130, 30);
        }

        if (dis_sensor(0) < 14 && dis_sensor(2) < 14)
        {
          rodar(130, 180);
        }
      }

      while (dis_sensor(2) < 15)
      {
        setup_motors(150, 0, 0, 35);
      }

      setup_motors(speed, 0, 0, speed);
    }
    else
    {
      if (distance < 0)
        return;

      while (dis_encoder(0) < distance && dis_encoder(1) < distance)
      {
        /*while (dis_sensor(0) < 15)
        {
          setup_motors(35, 0, 0, 150);
        }

        if (dis_sensor(1) <= 3)
        {
          while (dis_sensor(1) < 6)
          {
            setup_motors(0, 130, 130, 0);
          }

          if (dis_sensor(0) > dis_sensor(2))
          {
            rodar(130, -30);
          }
          else if (dis_sensor(2) > dis_sensor(0))
          {
            rodar(130, 30);
          }

          if (dis_sensor(0) < 14 && dis_sensor(2) < 14)
          {
            rodar(130, 180);
          }
        }

        while (dis_sensor(2) < 15)
        {
          setup_motors(150, 0, 0, 35);
        }*/

        setup_motors(speed, 0, 0, speed);
      }

      if (zero)
      {
        encoder.setPosition(0, 0L);
        encoder.setPosition(1, 0L);
      }
    }
    break;
  case 1:
    if (distance == 0)
    {
      setup_motors(0, speed, speed, 0);
    }
    else
    {
      if (distance < 0)
        return;

      while (dis_encoder(0) > -distance && dis_encoder(1) > -distance)
      {
        setup_motors(0, speed, speed, 0);
      }

      if (zero)
      {
        encoder.setPosition(0, 0L);
        encoder.setPosition(1, 0L);
      }
    }
    break;
  default:
    break;
  }
}

/**
   Método usado para andar com todas as velocidades parametrizadas e opção de distância
   @param in1 - velocidade a ser aplicada em In1
   @param in2 - velocidade a ser aplicada em In2
   @param in3 - velocidade a ser aplicada em In3
   @param in4 - velocidade a ser aplicada em In4
*/
void L298::andar(byte in1, byte in2, byte in3, byte in4, float distance)
{
  if (distance < 0)
    return;

  if (distance == 0)
  {
    setup_motors(in1, in2, in3, in4);
  }
  else
  {
    if (in1 != 0 && in4 != 0)
    {
      while (dis_encoder(0) < distance && dis_encoder(1) < distance)
      {
        setup_motors(in1, in2, in3, in4);
      }
    }
    else if (in1 != 0 && in4 == 0)
    {
      while (dis_encoder(1) < distance)
      {
        setup_motors(in1, in2, in3, in4);
      }
    }
    else if (in1 == 0 && in4 != 0)
    {
      while (dis_encoder(0) < distance)
      {
        setup_motors(in1, in2, in3, in4);
      }
    }
  }
}

/**
   Método usado para rodar o robô em torno do próprio eixo
   Roda o robô no sentido dos ponteiros se 'degree' for maior que zero
   Caso contrário, roda no sentido contrário dos ponteiros
   @param speed - velocidade a ser aplicada nos motores
   @param degree - ângulo a ser virado
*/
void L298::rodar(byte speed, short int degree)
{
  if (degree == 0)
    return;

  encoder.setPosition(0, 0L);
  encoder.setPosition(1, 0L);

  float toWalk = degree == 0 ? 0 : ((degree * 3.14 * 17.618561) / 180.0) / 2.0;

  if (degree > 0)
  {
    while (dis_encoder(0) < toWalk && dis_encoder(1) > -toWalk)
    {
      setup_motors(0, speed, 0, speed);
    }
  }
  else
  {
    while (dis_encoder(0) > toWalk && dis_encoder(1) < -toWalk)
    {
      setup_motors(speed, 0, speed, 0);
    }
  }
}

/**
   Método usado para virar para a esquerda ou direita, com ou sem uma angulação determinada
   @param direction - direção a se locomover
   @param speed - velocidade a ser aplicada nos motores
   @param degree - ângulo a ser virado
*/
void L298::virar(byte direction, byte speed, short int degree)
{
  float toWalk = degree == 0 ? 0 : (degree * 3.14 * 17.618561) / 180.0;

  encoder.setPosition(0, 0L);
  encoder.setPosition(1, 0L);

  switch (direction)
  {
  case 2:
    if (degree == 0)
    {
      setup_motors(speed, 0, 0, 0);
    }
    else
    {
      if (degree > 0)
      {
        while (dis_encoder(1) < toWalk)
        {
          setup_motors(speed, 0, 0, 0);
        }

        encoder.setPosition(1, 0L);
      }
      else
      {
        while (dis_encoder(1) > toWalk)
        {
          setup_motors(0, speed, 0, 0);
        }

        encoder.setPosition(1, 0L);
      }
    }
    break;

  case 3:
    if (degree == 0)
    {
      setup_motors(0, 0, 0, speed);
    }
    else
    {
      if (degree > 0)
      {
        while (dis_encoder(0) < toWalk)
        {
          setup_motors(0, 0, 0, speed);
        }

        encoder.setPosition(0, 0L);
      }
      else
      {
        while (dis_encoder(0) > toWalk)
        {
          setup_motors(0, 0, speed, 0);
        }

        encoder.setPosition(0, 0L);
      }
    }
    break;

  default:
    break;
  }
}

/**
   Método usado para parar o robô
*/
void L298::parar()
{
  setup_motors(0, 0, 0, 0);
}

/**
   Método usado para locomoção
   @param var1 - velocidade a ser aplicada em In1
   @param var2 - velocidade a ser aplicada em In2
   @param var3 - velocidade a ser aplicada em In3
   @param var4 - velocidade a ser aplicada em In4
*/
void L298::setup_motors(byte var1, byte var2, byte var3, byte var4)
{
  setup_motor(0, var1, var2);
  setup_motor(1, var3, var4);
}

/**
   Método usado para locomoção
   @param motor_index - motor a ser aplicada a velocidade
   @param var1 - velocidade a ser aplicada
   @param var2 - velocidade a ser aplicada
*/
void L298::setup_motor(byte motor_index, byte var1, byte var2)
{
  analogWrite(motors[motor_index].in1, var1);
  analogWrite(motors[motor_index].in2, var2);
}

/**
   Método usado para calcular a distância percorrida utilizando encoders
   @param encoder_index - encoder a ter a distância calculada
*/
float L298::dis_encoder(byte encoder_index)
{
  EncoderData data = datas[encoder_index];

  data.pos = 0.0;
  encoder.tick(encoder_index);
  data.npos = encoder.getPosition(encoder_index);

  if (data.pos != data.npos)
  {
    data.pos = data.npos;
    data.voltas = data.pos / 20.0;
  }

  return 2 * PI * 3.4 * data.voltas;
}

/**
   Método usado para retornar a distância de um dos sensores ultrasonicos
   @param sens - sensor a ter a distância 'medida'
*/
unsigned short int L298::dis_sensor(byte sens)
{
  return sensor.distancia(sens);
}

/**
   Método usado para obter uma média de distância percorrida entre os dois motores
*/
float L298::average()
{
  return (dis_encoder(0) + dis_encoder(1)) / 2.0;
}