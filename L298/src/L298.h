/*
 * Copyright (©)
 *
 * Projeto desenvolvido por Miguel Rodrigues
 * Não remova este quote
 *
 * Modificado em: 13/10/18 17:39
 */

#ifndef L298_h
#define L298_h

#include "Arduino.h"
#include "Ultrasonic.h"
#include "RotaryEncoder.h"

class L298
{
public:
  L298(byte in1, byte in2, byte in3, byte in4);

  void andar(byte direction, byte speed, float distance, bool zero);
  void andar(byte in1, byte in2, byte in3, byte in4, float distance);
  void virar(byte direction, byte speed, short int degree);
  void rodar(byte speed, short int degree);
  void parar();

  float dis_encoder(byte encoder_index);
  unsigned short int dis_sensor(byte sens);

  float average();

private:
  struct Motor;

  void setup_motors(byte var1, byte var2, byte var3, byte var4);
  void setup_motor(byte motor_index, byte var1, byte var2);
};

#endif
