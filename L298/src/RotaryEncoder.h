#ifndef RotaryEncoder_h
#define RotaryEncoder_h

#include "Arduino.h"

class RotaryEncoder
{
public:
  RotaryEncoder(byte pin1, byte pin2, byte pin3, byte pin4);

  float getPosition(byte encoder_index);

  int8_t getDirection(byte encoder_index);

  void setPosition(byte encoder_index, long newPosition);

  void tick(byte encoder_index);
  void tick();

private:
  struct Encoder;
};

#endif