/*
 * Copyright (©)
 *
 * Projeto desenvolvido por Miguel Rodrigues
 * Não remova este quote
 *
 * Modificado em: 13/10/18 17:39
 */

#ifndef Ultrasonic_h
#define Ultrasonic_h

#include "Arduino.h"

class Ultrasonic
{
public:
	Ultrasonic(byte trig1, byte echo1, byte trig2, byte echo2, byte trig3, byte echo3);

	unsigned short int cronometrar(byte sensor);

	unsigned short int converter(unsigned short int microsec);

	unsigned short int distancia(byte sensor);

private:
	struct Sensor;
};

#endif