/*
 * Copyright (©)
 *
 * Projeto desenvolvido por Miguel Rodrigues
 * Não remova este quote
 *
 * Modificado em: 13/10/18 17:39
 */

#include "Ultrasonic.h"

/**
 * 0 - esquerda
 * 1 - frontal
 * 2 - direita
*/

/**
 * Estrutura de dados do sensor
*/
struct Sensor
{
	byte trigPin;
	byte echoPin;
};

/**
 * Declaração da array para armazenar as estruturas de dados dos sensores
*/
Sensor sensors[3];

/**
 * Construtor da classe 'Ultrassonic'
 * @param trig1 - trig do sensor esquerdo
 * @param echo1 - echo do sensor esquerdo
 * @param trig2 - trig do sensor central
 * @param echo2 - echo do sensor central
 * @param trig3 - trig do sensor direito
 * @param echo3 - echo do sensor direito
*/
Ultrasonic::Ultrasonic(byte trig1, byte echo1, byte trig2, byte echo2, byte trig3, byte echo3)
{
	pinMode(trig1, OUTPUT);
	pinMode(trig2, OUTPUT);
	pinMode(trig3, OUTPUT);

	pinMode(echo1, INPUT);
	pinMode(echo2, INPUT);
	pinMode(echo3, INPUT);

	sensors[0].trigPin = trig1;
	sensors[0].echoPin = echo1;

	sensors[1].trigPin = trig2;
	sensors[1].echoPin = echo2;

	sensors[2].trigPin = trig3;
	sensors[2].echoPin = echo3;
}

/**
 * Método usado para medir a a distância de um determinado sensor
 * @param sensor - sensor a ter a distância medida
*/
unsigned short int Ultrasonic::distancia(byte sensor)
{
	return constrain(converter(cronometrar(sensor)), 2, 400);
}

/**
 * Método usado para converter tempo em distância
 * @param microseg - tempo em microsegundos a ser convertido em distância
*/
unsigned short int Ultrasonic::converter(unsigned short int microsec)
{
	return (microsec / 2.0) / 27.6233;
}

/**
 * Método usado para cronometrar o tempo em que ondas emitidas pelo sensor são captadas de volta
 * @param sensor - sensor a ter o tempo medido
*/
unsigned short int Ultrasonic::cronometrar(byte sensor)
{
	digitalWrite(sensors[sensor].trigPin, LOW);
	delayMicroseconds(2);
	digitalWrite(sensors[sensor].trigPin, HIGH);
	delayMicroseconds(10);
	digitalWrite(sensors[sensor].trigPin, LOW);

	return pulseIn(sensors[sensor].echoPin, HIGH);
}